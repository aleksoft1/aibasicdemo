﻿using UnityEngine;

public class ZombieStraightLine : MonoBehaviour
{
    public Vector3 destination = new Vector3(9, 0, 11);
    public float Speed = 1.0f;

    void LateUpdate()
    {
        transform.Translate(translation: destination.normalized * Speed * Time.deltaTime);
    }
}
