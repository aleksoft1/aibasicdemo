﻿using UnityEngine;

public class SlerpPush : MonoBehaviour {

    public float Speed = 1.0f;
    public float Accuracy = 1.0f;
    public Transform Goal;

    void LateUpdate()
    {
        Vector3 destination = new Vector3(
            this.Goal.position.x,
            this.transform.position.y,
            this.Goal.position.z);

       
        if (Vector3.Distance(this.transform.position, destination) > this.Accuracy)
        {
            this.transform.Translate(0, 0, this.Speed * Time.deltaTime);
        }
    }
}
